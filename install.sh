echo "${warning}***DELETE CURRENT CORE SOURCE***"
rm -rf $(pwd)/clevertap
rm -rf $(pwd)/src/@clevertap
rm -f $(pwd)/public/clevertap_sw.js

echo "${blue}***CLONING CLEVERTAP SRC...***"
git clone git@bitbucket.org:whiskey_mobiclix/clevertap.git

echo "${blue}***CREATING SHORTCUT TO APP SRC...***"
ln -sf $(pwd)/clevertap/sources $(pwd)/src/@clevertap
cp $(pwd)/clevertap/sources/sw.js $(pwd)/public/clevertap_sw.js

echo "${blue}***INSTALLING crypto-js...***"
npm i crypto-js

echo "${blue}***JUST ENJOY***"