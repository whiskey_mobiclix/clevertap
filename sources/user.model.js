import { DYNAMIC_COOKIE_ID_KEY } from "./utils.js";
import cookies from "./cookie.js";

class User{
	constructor({
		identity,
		mobileSubscriberType,
		networkOperator,
		aggregatorName,
		name,
		firstName,
		lastName,
		email,
		phone,
		product,
		gender,
		yearOfBirth,
		country,
		signUpMethod,
		signUpDateTime,
		profileImageUrl,
		subscriptionPackageName,
		subscriptionStatus,
		subscriptionType,
		cookieId,
		userId,
	}){
		this.identity = identity || "";
		this.mobileSubscriberType = mobileSubscriberType || "";
		this.networkOperator = networkOperator || "";
		this.aggregatorName = aggregatorName || "";
		this.name = name || "";
		this.firstName = firstName || "";
		this.lastName = lastName || "";
		this.email = email || "";
		this.phone = phone || "";
		this.product = product || "";
		this.gender = gender || "";
		this.yearOfBirth = yearOfBirth || "";
		this.country = country || "";
		this.signUpMethod = signUpMethod || "";
		this.signUpDateTime = signUpDateTime || "";
		this.profileImageUrl = profileImageUrl || "";
		this.subscriptionPackageName = subscriptionPackageName || "";
		this.subscriptionStatus = subscriptionStatus || "";
		this.subscriptionType = subscriptionType || "";
		this.userId = userId || "";
	}

	toObject(){
		const connection = navigator.connection || navigator.mozConnection || navigator.webkitConnection;
		const cookieId = cookies.get(DYNAMIC_COOKIE_ID_KEY);

		return {
			"Identity": this.identity,
			"Mobile Subscriber Type": this.mobileSubscriberType,
			"Network Type": connection.type || "Wifi",
			"Network Operator": this.networkOperator,
			"Aggregator Name": this.aggregatorName,
			"Name": this.name,
			"First Name": this.firstName,
			"Last Name": this.lastName,
			"Email": this.email,
			"Phone": this.phone,
			"Product": this.product,
			"Gender": this.gender || "M",
			"Year Of Birth": this.yearOfBirth,
			"Country": this.country,
			"Profile Image Url": this.profileImageUrl,
			"Photo": this.profileImageUrl,
			"Subscription Package Name": this.subscriptionPackageName,
			"Subscription Status": this.subscriptionStatus,
			"Subscription Type": this.subscriptionType,
			"Cookie Id": cookieId,
			"User Id": this.userId,
			"Platform": "Web",
			"Log In First Time Ever": "yes",
		}
	}
}

export default User