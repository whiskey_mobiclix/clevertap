import CleverTapUserModel from "./user.model.js"
import { LOGIN_FIRST_TIME_OF_THE_DAY } from "./utils.js"
import cookies from "./cookie.js"
import events from "./events.js"
import utils from "./utils.js"

export const setupWebPush = () => {
	window.clevertap.notifications.push({
     	"titleText":"Would you like to receive Notifications?",
	 	"bodyText":"We promise to only send you relevant content and give you updates on your transactions",
	 	"okButtonText":"Yes",
	 	"rejectButtonText":"No thanks",
	 	"okButtonColor":"#1d8eff",
	 	"askAgainTimeInSeconds": 5,
	});
}

export default {
	login(data) {
		if(window.clevertap != undefined){
			window.clevertap.onUserLogin.push({
			 	"Site": {
			 		...data,
			 		"Log In First Time Ever": "yes",
			 	},
			})

			setupWebPush();
		}
	},
	update(data) {
		if(window.clevertap != undefined){
			window.clevertap.profile.push({
				"Site": data,
			})
		}
	},
	loginWithUserData({data, product}){
		try{
			if(window.clevertap != undefined){
				const cleverTapUser = new CleverTapUserModel({
		            identity: data.user_id,
		            userId: data.user_id,
		            name: data.full_name,
		            firstName: data.first_name,
		            lastName: data.last_name,
		            email: data.email,
		            phone: data.mobile_number,
		            product: product,
		            gender: "M",
		            yearOfBirth: data.birthday ? new Date(data.birthday * 1000).toISOString() : "",
		            country: data.country,
		            signUpDateTime: !data.subscription_info ? "" : (new Date(data.subscription_info.subscription_date * 1000).toISOString() + " UTC"),
		            profileImageUrl: data.avatar_url,
		            subscriptionPackageName: !data.subscription_info ? "" : data.subscription_info.package_name,
		            subscriptionStatus: !data.subscription_info ? "" : data.subscription_info.status,
		            subscriptionType: !data.subscription_info ? "" : data.subscription_info.package_code,
		        })

				window.clevertap.onUserLogin.push({
					"Site": cleverTapUser.toObject(),
				})

				setupWebPush();

				const loggedInFirstTimeOfTheDay = cookies.get(LOGIN_FIRST_TIME_OF_THE_DAY);
				if(!loggedInFirstTimeOfTheDay || new Date(loggedInFirstTimeOfTheDay) < utils.getToDayTs()){
					events.pushLogInFirstTimeOfTheDay({product});
					cookies.set(LOGIN_FIRST_TIME_OF_THE_DAY, +new Date());
				}
			}
		} catch(e){
			console.log("Error when login CleverTap with User Data", e)
		}
	}
}