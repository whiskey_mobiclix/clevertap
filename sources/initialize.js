import CONFIGS, { FIREBASE_CONFIG, FCM_PUBLIC_KEY } from "./config.js"
import events from "./events.js"
import utils, { URL_BEFORE_CLOSING_KEY, DYNAMIC_COOKIE_ID_KEY } from "./utils.js"
import cookies from "./cookie.js"
import profiles from "./profiles.js";
import UserModel from "./user.model.js";
import axios from "axios"

export const addCleverTapScript = ({product}) => {
	var script = document.createElement('script')
	script.type = 'text/javascript'
	script.innerHTML = `
		var clevertap = {event:[], profile:[], region : '${CONFIGS.CLEVERTAP_REGION}', account:[], onUserLogin:[], notifications:[],privacy:[]};
		clevertap.account.push({"id": "${CONFIGS.CLEVERTAP_ACCOUNT_ID[product]}"});
		(function () {
			var wzrk = document.createElement('script');
			wzrk.type = 'text/javascript';
			wzrk.async = true;
			wzrk.src = ('https:' == document.location.protocol ? 'https://d2r1yp2w7bby2u.cloudfront.net' : 'http://static.clevertap.com') + '/js/a.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wzrk, s);
		})();
	`
	document.getElementsByTagName('head')[0].appendChild(script);
}

export const addFireBaseAppScript = () => {
	var script = document.createElement('script')
	script.type = 'text/javascript'
	script.src = "https://www.gstatic.com/firebasejs/7.19.1/firebase-app.js"
	document.getElementsByTagName('head')[0].appendChild(script);
}

export const addFireBaseMessagingScript = () => {
	var script = document.createElement('script')
	script.type = 'text/javascript'
	script.src = "https://www.gstatic.com/firebasejs/7.19.1/firebase-messaging.js"
	document.getElementsByTagName('head')[0].appendChild(script);
}

export const initializeFireBase = ({callback}) => {
	var intervalHandler = setInterval(() => {
		if(window.firebase != undefined){
			clearInterval(intervalHandler);

			window.firebase.initializeApp(FIREBASE_CONFIG);

		    const messaging = window.firebase.messaging();
		    messaging.usePublicVapidKey(FCM_PUBLIC_KEY);

		    callback();
		}
	}, 500);
}

export const generateCookiesId = ({trace, product}) => {
	try{
		const ip = trace.match(/ip=.+\n/)[0].replace("ip=", "")

		var dynamicCookieId = cookies.get(DYNAMIC_COOKIE_ID_KEY);
		if(!dynamicCookieId){
			dynamicCookieId = utils.generateDynamicCookieId({ipAddress: ip});
			
			//Push a new user profile to with Identity = Dynamic Cookie Id to CleverTap
			profiles.login(new UserModel({
				identity: dynamicCookieId,
				product: product,
			}).toObject());
		}
	} catch (e){
		console.log("Error on get IP address to generate Cookie ID", e);
	}
}

export const handleClosingTabOrWindow = ({trace, product}) => {
	/* Store last screen url to local storage when disposed */
	window.onunload = function (event) {
		localStorage.setItem(URL_BEFORE_CLOSING_KEY, JSON.stringify({
			url: window.location.href,
			time: +new Date(),
		}));
	};

	/* Push 'Last Screen Before Closing Web' event if stored in local storage */
	const urlBeforeClosing = localStorage.getItem(URL_BEFORE_CLOSING_KEY);
	if(urlBeforeClosing){
		try{
			const urlClosingTime = JSON.parse(urlBeforeClosing).time;

			if(+new Date() - urlClosingTime > 5000) {
				try{
					const loc = trace.match(/loc=.+\n/)[0].replace("loc=", "")

					events.pushLastScreenBeforeClosingWeb({
						productName: product,
						country: loc,
						pageUrl: JSON.parse(urlBeforeClosing).url,
					});

					localStorage.removeItem(URL_BEFORE_CLOSING_KEY)
				} catch(e){
					console.log(e)
				}
			} else {
				localStorage.removeItem(URL_BEFORE_CLOSING_KEY);
			}
		} catch(e){
			console.log("Error when push Last Screen Before Closing Web event", e);
		}
	}
}

export default {
	async inject({product}){
		if(typeof window != undefined){
			addCleverTapScript({product});
			addFireBaseAppScript();
			addFireBaseMessagingScript();
			initializeFireBase({
				callback: async () => {
					const traceResponse = await axios({
						method: "get",
						url: "https://www.cloudflare.com/cdn-cgi/trace",
						timeout: 5000,
					});

					generateCookiesId({trace: traceResponse.data, product});
					handleClosingTabOrWindow({trace: traceResponse.data, product});
				}
			});
		}
	}
}