export default {
	CLEVERTAP_ACCOUNT_ID: {
		acronis: "TEST-KK4-64Z-WW6Z",
		vanguard: "4Z7-74Z-WW6Z",
		gameon: "886-64Z-WW6Z",
		babystep: "6Z7-74Z-WW6Z",
		mocoplay: "8Z7-74Z-WW6Z",
		moco4k: "W48-74Z-WW6Z",
		wwe: "R48-74Z-WW6Z",
		hopster: "448-74Z-WW6Z",
		reboundtv: "RZ7-74Z-WW6Z",
		cricketon: "485-74Z-WW6Z",
		mocovids: "648-74Z-WW6Z",
	},
	CLEVERTAP_REGION: "sg1",
	DYNAMIC_COOKIE_ID_TIMEOUT_IN_MINUTE: 365 * 24 * 60,
}

export const CLEVERTAP_EVENTS = {
	forgetPasswordClicked: "Forget Password Clicked",
	signUpInitiated: "Sign-up Initiated",
	signUpSuccessful: "Sign-up Successful",
	paymentSuccesful: "Payment Successful",
	paymentFailed: "Payment Failed",
	lastScreenBeforeClosingWeb: "Last Screen Before Closing Web",
	logInSuccesful: "Log In Successful",
	exampleEvent: "Test event by Web SDK",
	playFirstGameOfTheDay: "Play First Game Of The Day",
	playFirstVideoOfTheDay: "Play First Video Of The Day",
	logInFirstTimeOfTheDay: "Log In First Time Of The Day",
}

export const FIREBASE_CONFIG = {
  	apiKey: "AIzaSyD3ACSLK6teZRMauFWu5CBKX4Ga4UZtKaE",
    authDomain: "scoth-6aeb2.firebaseapp.com",
    databaseURL: "https://scoth-6aeb2.firebaseio.com",
    projectId: "scoth-6aeb2",
    storageBucket: "scoth-6aeb2.appspot.com",
    messagingSenderId: "1066437920921",
    appId: "1:1066437920921:web:9d1ff9aff93dcff51fd008",
    measurementId: "G-TTYT12V10Z"
}

export const FCM_PUBLIC_KEY = "BIuyH_iw7wjQyVHg48VyuAJGjoWuzw8AEwmaiTaoLa3rfDG7a-_RLSe1EH_jWd5Igsk9Xt3rDx1o-8qA_IbHnMQ"