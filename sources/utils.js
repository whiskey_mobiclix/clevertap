import cookies from "./cookie.js";
import cleverTapConfigs from "./config.js";

export const STATIC_COOKIE_ID_KEY = "mw-clevertap-static-cookie-id";
export const DYNAMIC_COOKIE_ID_KEY = "mw-clevertap-dynamic-cookie-id"
export const HAS_JUST_LOGGED_IN = "mw-has-just-logged-in";
export const URL_BEFORE_CLOSING_KEY = "mw-url-before-closing"
export const SECRET_KEY = "weventure";
export const PLAY_FIRST_GAME_OF_THE_DAY = "mw-play-first-game-of-the-day";
export const PLAY_FIRST_VIDEO_OF_THE_DAY = "mw-play-first-video-of-the-day";
export const LOGIN_FIRST_TIME_OF_THE_DAY = "mw-login-first-time-of-the-day";

var CryptoJS = require("crypto-js");

export default {
	generateDynamicCookieId({ipAddress}){
		var existedCookieId = cookies.get(DYNAMIC_COOKIE_ID_KEY);
		if(!existedCookieId){
			const userAgent = navigator.userAgent
			const newCookieId =  CryptoJS.HmacMD5(ipAddress + userAgent + (+new Date()), SECRET_KEY).toString()
			cookies.set(
				DYNAMIC_COOKIE_ID_KEY,
				newCookieId,
				cleverTapConfigs.DYNAMIC_COOKIE_ID_TIMEOUT_IN_MINUTE * 60 * 1000
			);
			existedCookieId = newCookieId;
		}

		return existedCookieId;
	},
	generateStaticCookieId({ipAddress}){
		var existedCookieId = cookies.get(STATIC_COOKIE_ID_KEY);
		if(!existedCookieId){
			const userAgent = navigator.userAgent
			const newCookieId =  CryptoJS.HmacMD5(ipAddress + userAgent, SECRET_KEY).toString()
			cookies.set(STATIC_COOKIE_ID_KEY, newCookieId);
			existedCookieId = newCookieId;
		}

		return existedCookieId;
	},
	getToDayTs(){
		const year = new Date().getYear();
		const month = new Date().getMonth();
		const date = new Date().getDate();

		return +new Date(year, month, date);
	}
}