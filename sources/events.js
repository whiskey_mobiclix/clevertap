import CLEVERTAP_CONFIG, { CLEVERTAP_EVENTS } from "./config"

export const push = data => {
	if(!data || typeof data != "object") return

	const { name, props } = data

	if(window.clevertap != undefined){
		window.clevertap.event.push(name, props);
	}
}

export default {
	pushForgetPasswordClick({product}){
		push({
			name: CLEVERTAP_EVENTS.forgetPasswordClicked,
			props: {
				"product": product,
			}
		})
	},
	pushLogInSuccesful({product}){
		push({
			name: CLEVERTAP_EVENTS.logInSuccesful,
			props: {
				"product": product,
			}
		})
	},
	pushExampleEvent(){
		push({
			name: CLEVERTAP_EVENTS.exampleEvent,
			props: {}
		})
	},
	pushSignUpInitiated({product}){
		push({
			name: CLEVERTAP_EVENTS.signUpInitiated,
			props: {
				"product": product,
			}
		})
	},
	pushSignUpSuccessful({product}){
		push({
			name: CLEVERTAP_EVENTS.signUpSuccessful,
			props: {
				"product": product,
			}
		})
	},
	pushPaymentSuccesful({
		lastScreen,
		paymentType,
		packageName,
		paymentValue,
		currency,
		storeProductId,
		productName,
		paymentMethod,
	}){
		push({
			name: CLEVERTAP_EVENTS.paymentSuccesful,
			props: {
				"last screen": lastScreen,
				"payment type": paymentType,
				"package name": packageName,
				"payment value": paymentValue,
				"currency": currency,
				"store product id": storeProductId,
				"product": productName,
				"payment method": paymentMethod,
			}
		})
	},
	pushPaymentFailed({
		lastScreen,
		paymentType,
		packageName,
		paymentValue,
		currency,
		storeProductId,
		productName,
		paymentMethod,
	}){
		push({
			name: CLEVERTAP_EVENTS.paymentFailed,
			props: {
				"last screen": lastScreen,
				"payment type": paymentType,
				"package name": packageName,
				"payment value": paymentValue,
				"currency": currency,
				"store product id": storeProductId,
				"product": productName,
				"payment method": paymentMethod,
			}
		})
	},
	pushLastScreenBeforeClosingWeb({
		productName,
		country,
		pageUrl,
	}){
		push({
			name: CLEVERTAP_EVENTS.lastScreenBeforeClosingWeb,
			props: {
				"product": productName,
				"product name": productName,
				"country": country,
				"page url": pageUrl,
			}
		})
	},
	pushPlayFirstGameOfTheDay({
		gameId,
		product,
	}){
		push({
			name: CLEVERTAP_EVENTS.playFirstGameOfTheDay,
			props: {
				"product": product,
				"game id": gameId,
				"timestamp": +new Date(), 
			}
		})
	},
	pushLogInFirstTimeOfTheDay({
		product
	}){
		push({
			name: CLEVERTAP_EVENTS.logInFirstTimeOfTheDay,
			props: {
				"product": product,
				"timestamp": +new Date(), 
			}
		})
	},
	pushPlayFirstVideoOfTheDay({
		product,
		videoId,
	}){
		push({
			name: CLEVERTAP_EVENTS.playFirstVideoOfTheDay,
			props: {
				"product": product,
				"video id": videoId,
				"timestamp": +new Date(), 
			}
		})
	}
}