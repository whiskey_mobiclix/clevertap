export default {
	get(name){
		const value = `; ${document.cookie}`;
		const parts = value.split(`; ${name}=`);
		if (parts.length === 2) return parts.pop().split(';').shift();
	},
	set(name,value,secs) {
	    var expires = "";
	    if (secs) {
	        var date = new Date();
	        date.setTime(date.getTime() + (secs*1000));
	        expires = "; expires=" + date.toUTCString();
	    }
	    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
	},
	remove(name){
		document.cookie = name + "=;path=;domain=;expires=Thu, 01 Jan 1970 00:00:01 GMT";
	},
	removeAll(){
		var cookies = document.cookie.split(";");

	    for (var i = 0; i < cookies.length; i++) {
	        var cookie = cookies[i];
	        var eqPos = cookie.indexOf("=");
	        var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
	        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	    }
	}
}